HanbaiApp

Hanbai es una demo para mostrar los conocimientos aprendidos a lo largo del curso.
Se ha usuado: HTML5, CSS3, BOOTSTRAP, JQUERY, AJAX, NODEJS, EXPRESS, EJS, MYSQL.
En la primera versión usé PHP.

Instalación del proyecto

1. Creamos una base de datos mySQL llamada "hanbai" e importamos hanbai.sql
2. Abrimos la carpeta del proyecto y ejecutamos npm install
3. Después de la instalación de los módulos necesarios ejecutamos npm start
4. Abrimos en nuestro navegador: localhost:3000
5. Funcionando! 

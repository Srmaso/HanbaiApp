import express from "express";
import session from "express-session";
import user from "./controllers/user";
import http from "http";
import path from "path";
import controllers from './controllers/'
import {
  urlencoded as _urlencoded,
  json as _json
} from "body-parser";

var app = express();


//--------------------------------------------- Conexión a base de datos ------------------------------------------------------

import mysql from './db/conexion'

//--------------------------------------------- Definiendo vistas ------------------------------------------------------

app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use('/fonts', express.static('./node_modules/font-awesome/fonts'))

//--------------------------------------------- Cadena JSON ------------------------------------------------------

app.use(_urlencoded({
  extended: false
}));
app.use(_json());

//--------------------------------------------- Acceso a carpeta public para recursos (CSS, imágenes, JS...) ------------------------------------------------------


app.use(express.static(path.join(__dirname + '/public')));

//--------------------------------------------- Sesion de usuarios ------------------------------------------------------

app.use(session({
  secret: 'jumanji',
  resave: false,
  saveUninitialized: true,
  cookie: {
    maxAge: 3600000
  }
}))

//--------------------------------------------- Rutas ------------------------------------------------------

app.get('/', user.index);

//--------------------------------------------- API ------------------------------------------------------

app.get('/api/productos', controllers.listadoProductos);
app.get('/api/listadousuarios', controllers.listadoUsuarios);
app.post('/api/proceso', controllers.finalizarCompra);
app.get('/api/subirproductos', controllers.subirProductos);
app.post('/api/subirproductos', controllers.subirProductos);
app.post('/api/borrarproductos', controllers.borrarProductos);
app.post('/api/editarproductos', controllers.editarProductos);
app.get('/api/historialventas', controllers.historialVentas);
app.get('/api/numeroventas', controllers.numeroVentas);
app.get('/api/recaudaciontotal', controllers.recaudacionTotal);
app.get('/api/estadistica', controllers.estadistica);
app.get('/api/estadisticasRecaudacion', controllers.estadisticasRecaudacion);
app.get('/api/subirusuario', controllers.subirUsuario);
app.post('/api/subirusuario', controllers.subirUsuario);
app.post('/api/editarusuario', controllers.editarUsuario);
app.post('/api/borrarusuario', controllers.borrarUsuario);
app.get('/api/topvendedores', controllers.topVendedores);
app.get('/api/topproductos', controllers.topProductos);

//--------------------------------------------- Usuario ------------------------------------------------------

app.get('/login', user.index);
app.get('/signup', user.signup);
app.post('/signup', user.signup);
app.post('/login', user.login);
app.get('/logout', user.logout);

//--------------------------------------------- App ------------------------------------------------------

app.get('/panel', user.panel);
app.get('/productos', user.productos);
app.get('/historial', user.historial);
app.get('/estadisticas', user.estadisticas);
app.get('/gestionusuarios', user.gestionusuarios);


//--------------------------------------------- Puerto 3000 ------------------------------------------------------

app.set('port', process.env.PORT || 3000);

app.listen(3000, function () {
  console.log('Funcionando desde puerto 3000!');
});
//------------------------------------Productos más vendidos----------------------------------------------

import mysql from '../../db/conexion'

var connection = mysql.connection


export default (function (req, res) {

  mysql.connection.query('SELECT producto, COUNT( producto) maximo FROM ventaproducto GROUP BY producto ORDER BY maximo DESC LIMIT 6 ', function (error, results, fields) {
    if (error) throw error;

    res.setHeader('Content-Type', 'application/json')
    res.send(JSON.stringify(results))

  });

})
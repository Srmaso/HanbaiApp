// Usuarios con mayores ventas (no hay límite)

import mysql from '../../db/conexion'

var connection = mysql.connection


export default (function (req, res) {

  mysql.connection.query('SELECT usuario, sum(total) AS total from resultados GROUP BY usuario ORDER BY total DESC LIMIT 5', function (error, results, fields) {
    if (error) throw error;

    res.setHeader('Content-Type', 'application/json')
    res.send(JSON.stringify(results))

  });

})






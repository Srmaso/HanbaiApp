import listadoProductos from './productos/listadoProductos'
import listadoUsuarios from './usuarios/listadoUsuarios'
import finalizarCompra from './productos/finalizarCompra'
import subirProductos from './productos/subirProductos'
import borrarProductos from './productos/borrarProductos'
import editarProductos from './productos/editarProductos'
import historialVentas from './estadisticas/historialVentas'
import numeroVentas from './estadisticas/numeroVentas'
import recaudacionTotal from './estadisticas/recaudacionTotal'
import estadistica from './estadisticas/estadistica'
import estadisticasRecaudacion from './estadisticas/estadisticasRecaudacion'
import subirUsuario from './usuarios/subirUsuario'
import editarUsuario from './usuarios/editarUsuario'
import borrarUsuario from './usuarios/borrarUsuario'
import topVendedores from './estadisticas/topVendedores'
import topProductos from './estadisticas/topProductos'


exports.listadoProductos = listadoProductos;
exports.listadoUsuarios = listadoUsuarios;
exports.finalizarCompra = finalizarCompra;
exports.subirProductos = subirProductos;
exports.borrarProductos = borrarProductos;
exports.editarProductos = editarProductos;
exports.historialVentas = historialVentas;
exports.numeroVentas = numeroVentas;
exports.recaudacionTotal = recaudacionTotal;
exports.estadistica = estadistica;
exports.estadisticasRecaudacion = estadisticasRecaudacion;
exports.subirUsuario = subirUsuario;
exports.editarUsuario = editarUsuario;
exports.borrarUsuario = borrarUsuario;
exports.topVendedores = topVendedores;
exports.topProductos = topProductos;
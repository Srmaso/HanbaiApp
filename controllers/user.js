import mysql from '../db/conexion'

var connection = mysql.connection
var crypto = require('crypto');

//--------------------------------------------- Registro ------------------------------------------------------
exports.signup = function (req, res) {
    var messageErrorCredenciales = false;
    var messageErrorClave = false;
    var messageRegistro = false;
    var post = req.body;
    var clave = post.clave;
    if (clave !== '123') {
        messageErrorClave = true;
        res.render('index.ejs', { messageErrorCredenciales: messageErrorCredenciales, messageErrorClave: messageErrorClave, messageRegistro: messageRegistro });
        return;
    }
    if (req.method == "POST") {


        var name = post.name;
        var email = post.email;
        var password = post.password;
        var encriptado = crypto.createHash('md5').update(password).digest('hex');
        console.log(encriptado);


        var sql = "INSERT INTO `users`(`name`,`email`,`password`,`estado`) VALUES ('" + name + "','" + email + "','" + encriptado + "','" + '1' + "')";

        var query = mysql.connection.query(sql, function (err, result) {

            messageRegistro = true;
            res.render('index.ejs', { messageErrorCredenciales: messageErrorCredenciales, messageErrorClave: messageErrorClave, messageRegistro: messageRegistro });
        });

    } else {
        res.render('index.ejs', { messageErrorCredenciales: messageErrorCredenciales, messageErrorClave: messageErrorClave, messageRegistro: messageRegistro });
    }
};

//-----------------------------------------------Login------------------------------------------------------
exports.login = function (req, res) {
    var messageErrorCredenciales = false;
    var messageErrorClave = false;
    var messageRegistro = false;
    var sess = req.session;
    if (req.method == "POST") {
        var post = req.body;
        var email = post.user_email;
        var pass = post.password;
        var encriptado = crypto.createHash('md5').update(pass).digest('hex');

        var sql = "SELECT * FROM `users` WHERE `email`='" + email + "' and password = '" + encriptado + "'";
        mysql.connection.query(sql, function (err, results) {
            if (results.length) {
                req.session.userId = results[0].id;
                req.session.user = results[0];
                console.log(results[0].id);
                res.redirect('/panel');
            }
            else {
                messageErrorCredenciales = true;
                res.render('index.ejs', { messageErrorCredenciales: messageErrorCredenciales, messageErrorClave: messageErrorClave, messageRegistro: messageRegistro });
            }

        });
    } else {
        res.render('index.ejs', { messageErrorCredenciales: messageErrorCredenciales, messageErrorClave: messageErrorClave, messageRegistro: messageRegistro });
    }

};
//-----------------------------------------------Panel principal de productos----------------------------------------------

exports.panel = function (req, res, next) {

    var user = req.session.user,
        userId = req.session.userId;
    console.log('usuarioid=' + userId);
    if (userId == null) {
        res.redirect("/login");
        return;
    }

    var sql = "SELECT * FROM `users` WHERE `id`='" + userId + "'";

    mysql.connection.query(sql, function (err, results) {
        res.render('panel.ejs', { user: user, page_name: 'panel' });


    });
};
//------------------------------------Cerrar sesión----------------------------------------------
exports.logout = function (req, res) {
    req.session.destroy(function (err) {
        res.redirect("/login");
    })
};

//------------------------------------Edición de productos----------------------------------------------

exports.productos = function (req, res) {

    var user = req.session.user,
        userId = req.session.userId;
    console.log('usuarioid=' + userId);
    if (userId == null) {
        res.redirect("/login");
        return;
    }

    if (user.estado == 1) {
        res.redirect("/panel");
        return;
    }

    var sql = "SELECT * FROM `users` WHERE `id`='" + userId + "'";

    mysql.connection.query(sql, function (err, results) {
        res.render('productos.ejs', { user: user, page_name: 'productos' });


    });
};

//------------------------------------Historial----------------------------------------------

exports.historial = function (req, res) {

    var user = req.session.user,
        userId = req.session.userId;
    console.log('usuarioid=' + userId);
    if (userId == null) {
        res.redirect("/login");
        return;
    }

    if (user.estado == 1) {
        res.redirect("/panel");
        return;
    }

    var sql = "SELECT * FROM `users` WHERE `id`='" + userId + "'";

    mysql.connection.query(sql, function (err, results) {
        res.render('historial.ejs', { user: user, page_name: 'historial' });


    });
};

//------------------------------------Estadísticas----------------------------------------------

exports.estadisticas = function (req, res) {

    var user = req.session.user,
        userId = req.session.userId;
    console.log('usuarioid=' + userId);
    if (userId == null) {
        res.redirect("/login");
        return;
    }

    if (user.estado == 1) {
        res.redirect("/panel");
        return;
    }

    var sql = "SELECT * FROM `users` WHERE `id`='" + userId + "'";

    mysql.connection.query(sql, function (err, results) {
        res.render('estadisticas.ejs', { user: user, page_name: 'estadisticas' });


    });
};

//------------------------------------Pandel de admin para usuarios----------------------------------------------

exports.gestionusuarios = function (req, res) {

    var user = req.session.user,
        userId = req.session.userId;
    console.log('usuarioid=' + userId);
    if (userId == null) {
        res.redirect("/login");
        return;
    }

    if (user.estado == 1) {
        res.redirect("/panel");
        return;
    }

    var sql = "SELECT * FROM `users` WHERE `id`='" + userId + "'";

    mysql.connection.query(sql, function (err, results) {
        res.render('gestionusuarios.ejs', { user: user, page_name: 'gestionusuarios' });


    });
};

//------------------------------------Mensajes errores----------------------------------------------

exports.index = function (req, res) {
    var messageErrorCredenciales = false;
    var messageErrorClave = false;
    var messageRegistro = false;

    res.render('index', { messageErrorCredenciales: messageErrorCredenciales, messageErrorClave: messageErrorClave, messageRegistro: messageRegistro });

};


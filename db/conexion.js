//--------------------------------------------- Driver MySQL ------------------------------------------------------

import mysql from 'mysql';

//--------------------------------------------- Realizamos la conexión------------------------------------------------------

var connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'hanbai'
});

//--------------------------------------------- Mensaje de éxito o error------------------------------------------------------

connection.connect(function (err) {
  if (err) {
    console.error('Error de conexión: ' + err.stack);
    return;
  }

  console.log('Conectado a la base de datos ');
});

//--------------------------------------------- Exportamos conexión ------------------------------------------------------

exports.connection = connection;
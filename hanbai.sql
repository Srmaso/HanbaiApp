-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 30-05-2018 a las 14:18:38
-- Versión del servidor: 5.7.19
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `hanbai`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

DROP TABLE IF EXISTS `productos`;
CREATE TABLE IF NOT EXISTS `productos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `precio` float DEFAULT '0',
  `fotografia` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=95 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `nombre`, `precio`, `fotografia`) VALUES
(1, 'Wok verduras/ternera', 9.26, '1.jpg'),
(2, 'Nachos', 7, '2.jpg'),
(3, 'Dulces', 2.55, '3.jpg'),
(4, 'Hamburguesas', 7.59, '4.jpg'),
(5, 'Ensalada pollo', 8.25, '5.jpg'),
(6, 'Tartaleta', 6.59, '6.jpg'),
(7, 'Bocadillos', 10, '7.jpg'),
(8, 'Sushi variado', 17, '8.jpg'),
(9, 'Wraps', 12, '9.jpg'),
(10, 'Ensalada', 6, '10.jpg'),
(88, 'Donuts chocolate', 4.99, '3.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resultados`
--

DROP TABLE IF EXISTS `resultados`;
CREATE TABLE IF NOT EXISTS `resultados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(50) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `fechaCompra` timestamp NOT NULL,
  PRIMARY KEY (`id`),
  KEY `usuario` (`usuario`)
) ENGINE=MyISAM AUTO_INCREMENT=173 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `resultados`
--

INSERT INTO `resultados` (`id`, `usuario`, `total`, `fechaCompra`) VALUES
(162, 'Juan Antonio', '10.09', '2018-05-30 11:01:04'),
(149, 'Juan Antonio', '12.10', '2018-05-28 07:17:58'),
(150, 'Juan Antonio', '16.32', '2018-05-28 14:03:54'),
(151, 'Juan Antonio', '19.80', '2018-05-29 07:46:57'),
(152, 'Juan Antonio', '9.00', '2018-05-29 07:48:03'),
(153, 'Juan Antonio', '9.25', '2018-05-29 15:20:17'),
(154, 'Juan Antonio', '7.00', '2018-05-30 10:29:45'),
(161, 'Juan Antonio', '16.25', '2018-05-30 11:00:27'),
(160, 'Juan Antonio', '16.25', '2018-05-30 10:59:41'),
(159, 'Juan Antonio', '73.59', '2018-05-30 10:59:01'),
(158, 'Juan Antonio', '18.75', '2018-05-30 10:52:23'),
(157, 'Juan Antonio', '12.00', '2018-05-30 10:46:42'),
(156, 'Juan Antonio', '17.00', '2018-05-30 10:45:37'),
(155, 'Juan Antonio', '8.25', '2018-05-30 10:44:55'),
(137, 'Juan Antonio', '156.09', '2018-05-23 08:08:00'),
(138, 'Juan Antonio', '20.57', '2018-05-23 09:50:28'),
(139, 'Juan Antonio', '198.44', '2018-05-23 14:08:35'),
(140, 'Juan Antonio', '18.15', '2018-05-23 14:28:39'),
(141, 'Juan Antonio', '36.30', '2018-05-24 07:17:23'),
(142, 'Juan Antonio', '43.56', '2018-05-24 09:07:44'),
(143, 'Juan Antonio', '20.57', '2018-05-24 09:29:20'),
(144, 'Juan Antonio', '18.15', '2018-05-24 10:19:13'),
(145, 'Juan Antonio', '75.02', '2018-05-25 07:06:54'),
(146, 'Noelia', '35.09', '2018-05-25 12:30:24'),
(147, 'Juan Antonio', '18.09', '2018-05-25 14:13:54'),
(148, 'LaMary', '20.57', '2018-05-25 14:26:16'),
(136, 'Juan Antonio', '18.15', '2018-05-23 07:29:07'),
(135, 'Juan Antonio', '75.02', '2018-05-22 10:57:55'),
(134, 'Juan Antonio', '75.02', '2018-05-22 10:13:18'),
(133, 'Daniel ', '75.02', '2018-05-21 15:19:55'),
(132, 'Juan Antonio', '54.45', '2018-05-21 09:03:11'),
(131, 'Juan Antonio', '14.52', '2018-05-21 08:49:03'),
(130, 'Juan Antonio', '24.20', '2018-05-21 08:12:20'),
(129, 'Juan Antonio', '54.45', '2018-05-17 09:19:45'),
(163, 'Juan Antonio', '38.25', '2018-05-30 11:02:03'),
(164, 'Juan Antonio', '14.50', '2018-05-30 11:03:21'),
(165, 'Juan Antonio', '19.25', '2018-05-30 11:03:53'),
(166, 'Juan Antonio', '29.00', '2018-05-30 11:06:00'),
(167, 'Juan Antonio', '21.50', '2018-05-30 11:06:48'),
(168, 'Juan Antonio', '21.50', '2018-05-30 11:08:01'),
(169, 'Juan Antonio', '34.00', '2018-05-30 11:08:46'),
(170, 'Juan Antonio', '31.50', '2018-05-30 11:09:17'),
(171, 'Juan Antonio', '6.00', '2018-05-30 12:09:36'),
(172, 'Juan Antonio', '5.00', '2018-05-30 12:21:13');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `email` varchar(60) NOT NULL,
  `password` varchar(400) NOT NULL,
  `estado` int(8) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `estado`) VALUES
(43, 'LaMary', 'mary@mary.com', 'b8e7be5dfa2ce0714d21dcfc7d72382c', 1),
(25, 'Juan Antonio', 'masini2002@hotmail.com', '6aef2943ac9981593380d9088d28405c', 0),
(42, 'Noelia', 'noelia2@noelia.com', '17d7cd52cd18e7bab99bb71de1669d95', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventaproducto`
--

DROP TABLE IF EXISTS `ventaproducto`;
CREATE TABLE IF NOT EXISTS `ventaproducto` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `producto` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ventaproducto`
--

INSERT INTO `ventaproducto` (`id`, `producto`) VALUES
(11, 'Dulces'),
(2, 'Wok verduras/ternera'),
(3, 'Nachos'),
(4, 'Dulces'),
(5, 'Nachos'),
(6, 'Dulces'),
(7, 'Wraps'),
(8, 'Nachos'),
(9, 'Bocadillos'),
(10, 'Sushi variado'),
(12, 'Wraps'),
(13, 'Sushi variado'),
(15, 'Dulces'),
(16, 'Dulces');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

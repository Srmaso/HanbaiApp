// Nos permite programar usando mejoras de ES6 
require('babel-register')
//  Emula un entorno completo de ES2015 +
require('babel-polyfill')
// Vamos a app.js
require('./app')
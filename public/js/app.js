$(document).ready(function () {

    //--------------------------------------------- Listar productos en panel ------------------------------------------------------

    $.ajax({
            type: 'GET',
            url: 'api/productos',
            dataType: 'json',
            contentType: 'application/json; charset=utf-8'
        })
        .done(function (result) {
            for (i = 0; i < result.length; i++) {
                jQuery('.listaDeProductos').append(`
            <div class="col col-xs-6 col-md-3 col-lg-2 item">
            <div class="productoCaja">
            <h2 class="nombreProducto">` + result[i].nombre + `</h2>` +
                    `<div class="precio"> ` +
                    `<p></p>` +
                    `<p class="preciotexto">` + result[i].precio + `<p class="preciotexto">€</p>
                    </div >` +
                    `<div class='img-container'>
                    <img class="productoimg" src="images/` + result[i].fotografia + `">
                        
                    </div><hr></div></div>
                    `);
            }
        })
        .fail(function (xhr, status, error) {
            console.log(error);
        })

    $("#filter").keyup(function () {

        //--------------------------------------------- Recuperar el texto del campo de entrada y restablecer el recuento a cero ------------------------------------------------------

        var filter = $(this).val(),
            count = 0;

        $(".listaDeProductos .item ").each(function () {

            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).fadeOut(0);

            } else {
                $(this).show();

                count++;
            }
        });

        //--------------------------------------------- Actualizar contador ------------------------------------------------------
        var numberItems = count;

        $("#filter-count").text("Encontrados: " + count);

        if (numberItems == 0) {


            $(".noResultado").css("display", "block");



        } else {
            $(".noResultado").css("display", "none");

        }
    });



});
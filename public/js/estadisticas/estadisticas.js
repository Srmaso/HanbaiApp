$(function () {

    var options;

    $.ajax({
        type: 'GET',
        url: 'api/estadistica',
        dataType: 'json',
        success: function (data) {
            var numero = [];
            for (i = 0; i < data.length; i++) {
                numero.push(data[i].total);
            }

            var data = {    
                series: [numero]
            };

            // line chart
            options = {
                height: "300px",
                showPoint: true,
                axisX: {
                    showGrid: false
                },
                lineSmooth: false,
            };

            new Chartist.Line('#estadistica5', data, options);


        }

    })

    $.ajax({
        type: 'GET',
        url: 'api/estadisticasRecaudacion',
        dataType: 'json',
        success: function (data) {
            var numero = [];
            for (i = 0; i < 10; i++) {
                numero.push(data[i].total);
            }

            var numero2 = [];
            for (i = 0; i < 10; i++) {
                numero2.push(data[i].total + "€");
            }

            var data = {
                labels: numero2.slice(0),
                series: [numero]
            };

            // bar chart
            options = {
                height: "300px",
                axisX: {
                    showGrid: false
                },
            };

            new Chartist.Bar('#estadistica4', data, options);


        }

    })

    $.ajax({
        type: 'GET',
        url: 'api/topVendedores',
        dataType: 'json',
        success: function (data) {
            var numero = [];
            for (i = 0; i < data.length; i++) {
                numero.push(data[i].total);
            }

            var numero2 = [];
            for (i = 0; i < data.length; i++) {
                numero2.push((data[i].usuario) + ' ' + (data[i].total + "€"));
            }

            var data = {
                labels: numero2.slice(0),
                series: [numero]
            };

            // bar chart
            options = {
                height: "300px",
                barThickness : 73,
                axisX: {
                    showGrid: true,
                    barPercentage: 0.4
                },
            };

            new Chartist.Bar('#estadistica2', data, options);


        }
    })

    $.ajax({
        type: 'GET',
        url: 'api/estadistica',
        dataType: 'json',
        success: function (data) {
            var suma = 0;
            var numero = [];
            for (i = 0; i < data.length; i++) {
                suma +=(data[i].total);
                numero.push(suma);
            }
            
            jQuery('.evolucionResultado').append(`
        
                <b>` + suma.toFixed(2) + ' € recaudados' + `</b>`);


            var data = {    
                series: [numero]
            };

            // line chart
            options = {
                height: "300px",
                showPoint: true,
                axisX: {
                    showGrid: false
                },
                lineSmooth: false,
            };

            new Chartist.Line('#estadistica3', data, options);


        }

    })

    $.ajax({
        type: 'GET',
        url: 'api/topproductos',
        dataType: 'json',
        success: function (data) {
            var numero = [];
            for (i = 0; i < data.length; i++) {
                numero.push(data[i].maximo);
            }

            var numero2 = [];
            for (i = 0; i < data.length; i++) {
                numero2.push((data[i].producto));
            }

            var data = {
                labels: numero2.slice(0),
                series: [numero]
            };

            // bar chart
            options = {
                height: "300px",
                barThickness : 73,
                axisX: {
                    showGrid: true,
                    barPercentage: 0.4
                },
            };

            new Chartist.Bar('#estadistica1', data, options);


        }
    })



});
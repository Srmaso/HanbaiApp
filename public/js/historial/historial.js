$(document).ready(function () {

    show_historial(); // Llamar función

  
//--------------------------------------------- Función mostrar historial en tabla ------------------------------------------------------
  
    function show_historial() {
        $.ajax({
            type: 'GET',
            url: 'api/historialventas',
            dataType: 'json',
            success: function (data) {
                var html = '';
                var i;
                for (i = 0; i < data.length; i++) {
                    var fecha = moment(data[i].fechaCompra).format('DD-MM-YYYY HH:mm');
                    html += '<tr class="fadeIn animated">' +
                        '<td>' + data[i].id + '</td>' +
                        '<td>' + data[i].usuario + '</td>' +
                        '<td>' + data[i].total + '€' + '</td>' +
                        '<td>' + fecha + '</td>' +

                        '</tr>';
                }
                $('#show_historial').append(html);
                $('#mydata').DataTable({
                    "order": [[0, "desc"]],
                    "lengthMenu": [[10, 20, 50, -1], [10, 20, 50, "Todo"]],
                    "dom": '<"dt-buttons"Bf><"clear">lirtp',
                    "paging": true,
                    "autoWidth": true,
                    "buttons": [
                        {
                            extend: 'excelHtml5',
                            text: '<i class="fa fa-file-text" aria-hidden="true"></i> Exportar Excel',
                            className: 'label label-success',
                            customize: function (xlsx) {
                                var source = xlsx.xl['workbook.xml'].getElementsByTagName('sheet')[0];
                                source.setAttribute('name', 'New Name');
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            text: '<i class="fa fa-file-text" aria-hidden="true"></i> Exportar PDF',
                            className: 'label label-success',
                        }
                    ],
                    "language": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": " ",
                        "sInfoEmpty": " ",
                        "sInfoFiltered": " ",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar: ",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "Último",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    }
                });

            }

        });
    }

})

//--------------------------------------------- Llamadas api para adiccionales para estadíticas ------------------------------------------------------

$.ajax({
    type: 'GET',
    url: 'api/numeroventas',
    dataType: 'json',
    dataType: 'json',
    success: function (data) {
        jQuery('.numeroventas').append(`
        
                <p>` + data[0].total + `</p>`);
    }
})

$.ajax({
    type: 'GET',
    url: 'api/topvendedores',
    dataType: 'json',
    dataType: 'json',
    success: function (data) {
        jQuery('.topVendedor').append(`
        
                <p>` + data[0].usuario + `</p>`);
    }
})

$.ajax({
    type: 'GET',
    url: 'api/recaudaciontotal',
    dataType: 'json',
    dataType: 'json',
    success: function (data) {

        var total = 0;
        for (var i = 0; i < data.length; i++) {
            total += data[i].total;
        }

        var redondeo = total.toFixed(2);

        jQuery('.ganancias').append(`
            
                    <p>` + redondeo + '€' + `</p>`);
    }
})





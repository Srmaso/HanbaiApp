// Preparando variables

var itemCarrito = 0;
var subTotal = 0;
const iva = 0.10; // 10%
var precioTotal = 0;
var productosVenta = [];

// Onload cuando la página web se ha cargado completamente en el navegador.

window.onload = function () {
	$('img').addClass('img-responsive');

	$('.img-container').append('<button class="btn btn-success">+</button>');

	// Boton añadir carrito

	$('.btn-add-cart, .img-container').click((e) => {
		//animation
		$(e.target).animateCss('pulse');
		// find out which item is clicked
		// if 'span' with cart symbol is clicked, then navigate one level up to the button
		let evento;
		if ($(e.target).is('span')) evento = $(e.target).parent();
		else evento = $(e.target);
		let nombre = evento.parent().parent().find('h2')[0].textContent;
		let precio = evento.parent().parent().find('p')[1].textContent;
		addToCart(nombre, precio);
	});

	$('#submit').click(() => {
		formSubmitted();
	});


}



// La función para añadir

function addToCart(name, price) {
	let priceNumber = parseFloat(price.slice(0));
	if (itemCarrito === 0) $('#cart').text(" ");
	let newDiv = $('<div class="cart-item"></div>');
	newDiv.text(name + ' ... ' + price + '' + '€');
	newDiv.append('<button class="btn btn-danger btn-xs" onclick="deleteItem(this)">X</button>');
	newDiv.append('<hr>');
	newDiv.attr('name', name);
	newDiv.attr('price', priceNumber);
	$('#cart').append(newDiv);
	newDiv.animateCss('bounceInRight');
	itemCarrito++;
	$('#cartItems').text(itemCarrito);
	subTotal += priceNumber;
	updatePrice();
}


function deleteItem(e) {
	let price = $(e.parentElement).attr('price');
	subTotal -= price;
	$(e.parentElement).animateCss('bounceOutRight');
	$(e.parentElement).remove();
	itemCarrito--;
	$('#cartItems').text(itemCarrito);
	updatePrice();
	cartLonelyText();
}

function cartLonelyText() {
	if (itemCarrito === 0)
		$('#cart').append('Añadir producto para venta.');
}

function updatePrice() {
	var totalSinIva = (subTotal.toFixed(2) / 1.10);
	var ivaTotal = (subTotal.toFixed(2) - totalSinIva.toFixed(2));
	$('#prices').empty();
	if (itemCarrito === 0) return;
	let newDiv = $('<div></div>');
	newDiv.append('<strong>Subtotal: ' + totalSinIva.toFixed(2) + '€' + '<br>');
	newDiv.append('<strong>IVA 10%: ' + ivaTotal.toFixed(2) + '€' + '<br>');
	newDiv.append('<strong><h3>Total : ' + (subTotal.toFixed(2)) + '€' + '</h3></strong>');

	newDiv.append('<button class="btn btn-info btn-block" onclick="openModal()">Finalizar compra</button>');

	$('#prices').append(newDiv);
	newDiv.animateCss('bounceInRight');
}


function cartToString() {
	var totalSinIva = (subTotal.toFixed(2) / 1.10);
	var ivaTotal = (subTotal.toFixed(2) - totalSinIva.toFixed(2));
	let itemsString = "<p><strong><i class='fa fa-cutlery' aria-hidden='true'></i> Restaurante Le Meurice</strong><br>";
	itemsString += " 228 Rue de Rivoli<br>75001 Paris, Francia<br>Tlf: +33 1 44 58 10 10<br><hr>";
	let cartItems = document.querySelectorAll('.cart-item');
	
	for (let item of cartItems) {
		itemsString = itemsString + item.getAttribute('name') + " ... " + item.getAttribute('price') + '€' + "<br><hr>";
		productosVenta.push(item.getAttribute('name'));
	};
	console.log(productosVenta);
	itemsString += '</p><p>Subtotal: ' + totalSinIva.toFixed(2) + '€' + '<br>';
	itemsString += 'IVA 10%: ' + ivaTotal.toFixed(2) + '€' + '<br><hr>'
	itemsString += '<h4>Total : <strong>' + (subTotal.toFixed(2)) + '€' + '</strong></p><h4>';

	

	return itemsString;

	

}



function openModal() {
	$('#cartContentsModal').html(cartToString());
	$('#myModal').modal('show');
}



function formSubmitted() {

console.log(productosVenta[0]);
	$.ajax({
			method: 'POST',
			url: 'api/proceso',
			dataType: 'json', // <-- add this
			contentType: 'application/json; charset=utf-8',
			data: JSON.stringify({
				resultado: (subTotal.toFixed(2)),
				productosVenta: productosVenta

			}),

		})
		.done(function (msg) {
			$('#myModal').modal('hide');
			precioTotal = 0;
			subTotal = 0;
			itemCarrito = 0;
			$('#cart').empty();
			$('#prices').empty();
			$('#cartItems').text(itemCarrito);
			cartLonelyText();
			sweetAlert("Venta registrada!", "Transacción realizada con éxito", "success");
		})
		.fail(function (xhr, status, error) {
			console.log(error);
		})


}

// Función para imprimir (Gracias Google)

function imprSelec(nombre) {
	var ficha = document.getElementById(nombre);
	var ventimp = window.open(' ', 'popimpr');
	ventimp.document.write(ficha.innerHTML);
	ventimp.document.close();
	ventimp.print();
	ventimp.close();
}


$.fn.extend({
	//		https://github.com/daneden/animate.css
	animateCss: function (animationName) {
		var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
		this.addClass('animated ' + animationName).one(animationEnd, function () {
			$(this).removeClass('animated ' + animationName);
		});
		return this;
	}
});

$(document).ready(function () {
    show_product(); // Llamar a todos los productos

    //--------------------------------------------- Función llamar productos y listarlo en tabla ------------------------------------------------------

    function show_product() {
        $.ajax({
            type: 'GET',
            url: 'api/productos',
            dataType: 'json',
            success: function (data) {
                var html = '';
                var i;
                for (i = 0; i < data.length; i++) {
                    html += '<tr class="infoProductos">' +
                        '<td>' + data[i].id + '</td>' +
                        '<td>' + data[i].nombre + '</td>' +
                        '<td>' + data[i].precio + '€' + '</td>' +
                        '<td>' + `<img class="productoimgSmall" src="images/` + data[i].fotografia + `">` + '</td>' +
                        '<td style="text-align:right;">' +
                        '<a href="javascript:void(0);" class="btn btn-info btn-sm item_edit" data-nombre="' + data[i].nombre + '" data-precio="' + data[i].precio + '" data-foto="' + data[i].fotografia + '">Editar</a>' + ' ' +
                        '<a href="javascript:void(0);" class="btn btn-danger btn-sm item_delete" data-nombre="' + data[i].id + '">Eliminar</a>' +
                        '</td>' +
                        '</tr>';
                }
                $('#show_data').html(html);


                setTimeout(function(){
                    $('#mydata').DataTable({
                        retrieve: true,
                        "order": [[ 0, "desc" ]],
                        "lengthMenu": [[6, 12, 24, -1], [6, 12, 24, "Todo"]],
                        "language": {
                            "sProcessing":    "Procesando...",
                            "sLengthMenu":    "Mostrar _MENU_ productos",
                            "sZeroRecords":   "No se encontraron resultados",
                            "sEmptyTable":    "Ningún dato disponible en esta tabla",
                            "sInfo":          " ",
                            "sInfoEmpty":     " ",
                            "sInfoFiltered":  " ",
                            "sInfoPostFix":   "",
                            "sSearch":        "Buscar: ",
                            "sUrl":           "",
                            "sInfoThousands":  ",",
                            "sLoadingRecords": "Cargando...",
                            "oPaginate": {
                                "sFirst":    "Primero",
                                "sLast":    "Último",
                                "sNext":    "Siguiente",
                                "sPrevious": "Anterior"
                            },
                            "oAria": {
                                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                            }
                        }
                    });
                  }, 10)
                
                
            }

        });
    }

//--------------------------------------------- Guardar ------------------------------------------------------


    $('#btn_save').on('click', function () {
        var nombre = $('#nombre').val();
        var precio = $('#precio').val();
        var foto = $('#foto').val();
        $.ajax({
            type: "POST",
            url: '/api/subirproductos',
            dataType: "JSON",
            data: { nombre: nombre, precio: precio, foto: foto },
            success: function (data) {
                $('[name="nombre"]').val("");
                $('[name="precio"]').val("");
                $('[name="foto"]').val("");
                $('#Modal_Add').modal('hide');
                location.reload();
            
            }
        });
        return false;
    });

//--------------------------------------------- Obtener datos para mostrar en edición ------------------------------------------------------

    $('#show_data').on('click', '.item_edit', function () {
        var nombre = $(this).data('nombre');
        var precio = $(this).data('precio');
        var foto = $(this).data('foto');

        $('#Modal_Edit').modal('show');
        $('[name="nombre_edit"]').val(nombre);
        $('[name="precio_edit"]').val(precio);
        $('[name="foto_edit"]').val(foto);
    });

//--------------------------------------------- Actualizar producto ------------------------------------------------------

    $('#btn_update').on('click', function () {
        var nombre = $('#nombre_edit').val();
        var precio = $('#precio_edit').val();
        var foto = $('#foto_edit').val();
        $.ajax({
            type: "POST",
            url: "/api/editarproductos",
            dataType: "JSON",
            data: { nombre: nombre, precio: precio, foto: foto },
            success: function (data) {
                $('[name="nombre_edit"]').val("");
                $('[name="precio_edit"]').val("");
                $('[name="foto_edit"]').val("");
                $('#Modal_Edit').modal('hide');
                location.reload();
            }
        });
        return false;
    });

    //--------------------------------------------- Dato para identificar borrador ------------------------------------------------------


    $('#show_data').on('click', '.item_delete', function () {
        var nombre = $(this).data('nombre');

        $('#Modal_Delete').modal('show');
        $('[name="nombre_delete"]').val(nombre);
    });

//--------------------------------------------- Borrar ------------------------------------------------------

    $('#btn_delete').on('click', function () {
        var nombre = $('#nombre_delete').val();
        $.ajax({
            type: "POST",
            url: "api/borrarproductos",
            dataType: "JSON",
            data: { nombre: nombre },
            success: function (data) {
                $('[name="nombre_delete"]').val("");
                $('#Modal_Delete').modal('hide');
                location.reload();
            }
        });
        return false;
    });

});


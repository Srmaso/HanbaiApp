
$(document).ready(function () {
    show_users(); //Llamar función usuarios
    
   //--------------------------------------------- Función mostrar usuarios en tabla ------------------------------------------------------
    function show_users() {
        $.ajax({
            type: 'GET',
            url: 'api/listadoUsuarios',
            dataType: 'json',
            success: function (data) {
                var html = '';
                var i;
                for (i = 0; i < data.length; i++) {

                    var roles;
                    if(data[i].estado == 1){
                        var roles = "Empleado";
                    }
                    else {
                        var roles = "<span class='administrador'>Administrador</span>";
                    }

                    html += '<tr>' +
                        '<td>' + data[i].name + '</td>' +
                        '<td>' + data[i].email + '</td>' +
                        '<td>' + roles + '</td>' +
                        '<td style="text-align:right;">' +
                        '<a href="javascript:void(0);" class="btn btn-info btn-sm item_edit" data-nombreusuario="' + data[i].name + '" data-emailusuario="' + data[i].email + '" data-estadousuario="' + data[i].estado + '">Editar</a>' + ' ' +
                        '<a href="javascript:void(0);" class="btn btn-danger btn-sm item_delete" data-nombreusuario="' + data[i].id + '">Eliminar</a>' +
                        '</td>' +
                        '</tr>';
                }
                $('#show_dataUsers').html(html);
                setTimeout(function(){
                    $('#mydata').DataTable({
                        retrieve: true,
                        "order": [[ 0, "asc" ]],
                        "lengthMenu": [[10, 20, 30, -1], [10, 20, 30, "Todo"]],
                        "language": {
                            "sProcessing":    "Procesando...",
                            "sLengthMenu":    "Mostrar _MENU_ usuarios",
                            "sZeroRecords":   "No se encontraron resultados",
                            "sEmptyTable":    "Ningún dato disponible en esta tabla",
                            "sInfo":          " ",
                            "sInfoEmpty":     " ",
                            "sInfoFiltered":  " ",
                            "sInfoPostFix":   "",
                            "sSearch":        "Buscar: ",
                            "sUrl":           "",
                            "sInfoThousands":  ",",
                            "sLoadingRecords": "Cargando...",
                            "oPaginate": {
                                "sFirst":    "Primero",
                                "sLast":    "Último",
                                "sNext":    "Siguiente",
                                "sPrevious": "Anterior"
                            },
                            "oAria": {
                                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                            }
                        }
                    });
                  }, 10)
            }

        });
    }

    //--------------------------------------------- Nuevo usuario ------------------------------------------------------

    $('#btn_saveUsuario').on('click', function () {
        var nombreusuario = $('#nombreusuario').val();
        var emailusuario = $('#emailusuario').val();
        var passusuario = $('#passusuario').val();
        $.ajax({
            type: "POST",
            url: '/api/subirusuario',
            dataType: "JSON",
            data: { nombreusuario: nombreusuario, emailusuario: emailusuario, passusuario: passusuario },
            success: function (data) {
                $('[name="nombreusuario"]').val("");
                $('[name="emailusuario"]').val("");
                $('[name="passusuario"]').val("");
                $('#Modal_Addusuario').modal('hide');
                location.reload();
            }
        });
        return false;
    });

//--------------------------------------------- Obtener datos para guardar ------------------------------------------------------

    $('#show_dataUsers').on('click', '.item_edit', function () {
        var nombreusuario = $(this).data('nombreusuario');
        var emailusuario = $(this).data('emailusuario');
        var estadousuario = $(this).data('estadousuario');

        $('#Modal_EditUsuario').modal('show');
        $('[name="nombreusuario_edit"]').val(nombreusuario);
        $('[name="emailusuario_edit"]').val(emailusuario);
        $('[name="estadousuario_edit"]').val(estadousuario);
    });

    //--------------------------------------------- Guardar usuario ------------------------------------------------------

    $('#btn_updateUsuario').on('click', function () {
        var nombreusuario = $('#nombreusuario_edit').val();
        var emailusuario = $('#emailusuario_edit').val();
        var estadousuario = $('#estadousuario_edit').val();
        $.ajax({
            type: "POST",
            url: "/api/editarusuario",
            dataType: "JSON",
            data: { nombreusuario: nombreusuario, emailusuario: emailusuario, estadousuario: estadousuario },
            success: function (data) {
                $('[name="nombreusuario_edit"]').val("");
                $('[name="emailusuario_edit"]').val("");
                $('[name="estadousuario_edit"]').val("");
                $('#Modal_EditUsuario').modal('hide');
                location.reload();
            }
        });
        return false;
    });

//--------------------------------------------- Obtener dato para eliminar------------------------------------------------------

    $('#show_dataUsers').on('click', '.item_delete', function () {
        var nombreusuario = $(this).data('nombreusuario');
        $('#Modal_Deleteusuario').modal('show');
        $('[name="nombreusuario_delete"]').val(nombreusuario);
    });



 //--------------------------------------------- Borrar usuario ------------------------------------------------------
    $('#btn_deleteUsuario').on('click', function () {
        var nombreusuario = $('#nombreusuario_delete').val();
        $.ajax({
            type: "POST",
            url: "api/borrarusuario",
            dataType: "JSON",
            data: { nombreusuario: nombreusuario },
            success: function (data) {
                $('[name="nombreusuario_delete"]').val("");
                $('#Modal_Deleteusuario').modal('hide');
                location.reload();
            }
        });
        return false;
    });

});